package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.TypeA;
import com.epam.rd.java.basic.task8.TypeB;
import com.epam.rd.java.basic.task8.TypeC;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public TypeA parse() throws XMLStreamException, FileNotFoundException {
        TypeA typeA = new TypeA();
        TypeB typeB = null;
        TypeC typeC = null;

        String xValue = null;

        XMLStreamReader xmlr = XMLInputFactory.newInstance().createXMLStreamReader(xmlFileName, new FileInputStream(xmlFileName));

        while (xmlr.hasNext()) {
            xmlr.next();
            if (xmlr.isStartElement()) {
                switch (xmlr.getLocalName()) {
                    case "b":
                        typeB = new TypeB();
                        typeA.getB().add(typeB);
                        break;
                    case "c":
                        typeC = new TypeC();
                        typeB.getC().add(typeC);
                        xValue = xmlr.getAttributeValue(0);
                        break;
                    case "d":
                        if (xmlr.hasName()) {
                            xmlr.next();
                            String text = xmlr.getText();
                            typeC.setD(text);
                        }
                        typeC.setX(Integer.parseInt(xValue));
                        break;
                }
            }
        }

        return typeA;
    }
}