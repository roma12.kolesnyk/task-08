package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.TypeA;
import com.epam.rd.java.basic.task8.TypeB;
import com.epam.rd.java.basic.task8.TypeC;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public TypeA parse() throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		final TypeA[] result = new TypeA[1];

		DefaultHandler handler = new DefaultHandler() {
			boolean d = false;

			TypeA typeA = new TypeA();
			TypeB typeB;
			TypeC typeC;


			// Метод вызывается когда SAXParser "натыкается" на начало тэга
			@Override
			public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
				if (qName.equalsIgnoreCase("b")) {
					typeB = new TypeB();
					typeA.getB().add(typeB);
				}
				if (qName.equalsIgnoreCase("c")) {
					typeC = new TypeC();
					typeB.getC().add(typeC);
					typeC.setX(Integer.parseInt(attributes.getValue("x")));
				}
				if (qName.equalsIgnoreCase("d")) {
					d = true;
				}
			}

			@Override
			public void endDocument() throws SAXException {
				result[0] = typeA;
			}

			// Метод вызывается когда SAXParser считывает текст между тэгами
			@Override
			public void characters(char[] ch, int start, int length) throws SAXException {
				if (d) {
					String s = new String(ch, start, length);
					typeC.setD(s);
					d = false;
				}
			}
		};

		// Стартуем разбор методом parse, которому передаем наследника от DefaultHandler, который будет вызываться в нужные моменты
		saxParser.parse(xmlFileName, handler);

		return result[0];
	}
}