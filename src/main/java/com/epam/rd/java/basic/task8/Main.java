package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }
        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);


        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        DOMController domController = new DOMController(xmlFileName);
        TypeA tpA = domController.parse();

        Comparator<TypeC> aComparator = (o1, o2) -> {
            String s1 = o1.getD();
            String s2 = o2.getD();
            return s1.compareToIgnoreCase(s2);
        };

        List<TypeC> ls = tpA.getB().get(0).getC();
        System.out.println("orig" + ls);
        ls.sort(aComparator);
        System.out.println("aft sort" + ls);

        writeToFile(tpA, "output.dom.xml");


        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        SAXController saxController = new SAXController(xmlFileName);
        tpA = saxController.parse();

        Comparator<TypeC> cComparator = Comparator.comparing(TypeC::getX).reversed();

        System.out.println("~~~");
        System.out.println(tpA);
        tpA.getB().get(0).getC().sort(cComparator);
        System.out.println(tpA);

        writeToFile(tpA, "output.sax.xml");

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        STAXController staxController = new STAXController(xmlFileName);
        tpA = staxController.parse();

        Comparator<TypeC> acComparator = Comparator.comparing(TypeC::getX).reversed().thenComparing(TypeC::getD);

        System.out.println("~~~");
        System.out.println(tpA);
        tpA.getB().get(0).getC().sort(cComparator);
        System.out.println(tpA);

        writeToFile(tpA, "output.stax.xml");
    }

    private static void writeToFile(TypeA tpA, String filename) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("ts:a");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://com.koles:8080/dr/dre input.xsd");
        rootElement.setAttribute("xmlns:ts", "http://com.koles:8080/dr/dre");
        doc.appendChild(rootElement);

        for (int i = 0; i < tpA.getB().size(); i++) {
            Node rootB = rootElement.appendChild(doc.createElement("b"));
            rootElement.appendChild(rootB);

            for (int j = 0; j < tpA.getB().get(i).getC().size(); j++) {
                Node rootC = rootB.appendChild(doc.createElement("c"));

                String text = tpA.getB().get(i).getC().get(j).getD();
                int numb = tpA.getB().get(i).getC().get(j).getX();

                ((Element) rootC).setAttribute("x", String.valueOf(numb));
                Node rootD = rootC.appendChild(doc.createElement("d"));
                rootD.appendChild(doc.createTextNode(text));
            }
        }


        // write dom document to a file
        try (FileOutputStream output = new FileOutputStream(filename)) {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(output);
            transformer.transform(source, result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

/*
class DomExample {

    public static void main(String[] args) {
        try {
            // Создается построитель документа
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            // Создается дерево DOM документа из файла
            Document document = documentBuilder.parse("input.xml");

            // Получаем корневой элемент
            Node root = document.getDocumentElement();

            System.out.println("List of books:");
            System.out.println();
            // Просматриваем все подэлементы корневого - т.е. книги
            NodeList books = root.getChildNodes();
            for (int i = 0; i < books.getLength(); i++) {
                Node book = books.item(i);
                // Если нода не текст, то это книга - заходим внутрь
                if (book.getNodeType() != Node.TEXT_NODE) {
                    NodeList bookProps = book.getChildNodes();
                    for (int j = 0; j < bookProps.getLength(); j++) {
                        Node bookProp = bookProps.item(j);
                        // Если нода не текст, то это один из параметров книги - печатаем
                        if (bookProp.getNodeType() != Node.TEXT_NODE) {
                            System.out.println(bookProp.getNodeName() + ":" + bookProp.getChildNodes().item(0).getTextContent());
                        }
                    }
                    System.out.println("===========>>>>");
                }
            }

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            ex.printStackTrace();
        }
    }
}*/
