package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.TypeA;
import com.epam.rd.java.basic.task8.TypeB;
import com.epam.rd.java.basic.task8.TypeC;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public TypeA parse() throws ParserConfigurationException, IOException, SAXException {

		DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = documentBuilder.parse(xmlFileName);

		Node root = document.getDocumentElement();

		NodeList aElements = root.getChildNodes();


		TypeA a = new TypeA();
		List<TypeB> bs = a.getB();

		for (int i = 0; i < aElements.getLength(); i++) {
			Node bNode = aElements.item(i);
			if (bNode.getNodeType() == Node.TEXT_NODE) continue;

			NodeList bElements = bNode.getChildNodes();

			TypeB curB = new TypeB();
			List<TypeC> cs = curB.getC();

			bs.add(curB);

			for (int j = 0; j < bElements.getLength(); j++) {

				Node cNode = bElements.item(j);
				if (cNode.getNodeType() == Node.TEXT_NODE) continue;



				NodeList cElements = cNode.getChildNodes();
				// get attrib x from c
				NamedNodeMap at = cNode.getAttributes();
				int x = Integer.parseInt(at.getNamedItem("x").getTextContent());

				for (int k = 0; k < cElements.getLength(); k++) {
					Node dNode = cElements.item(k);
					if (dNode.getNodeType() == Node.TEXT_NODE) continue;

					String textD = dNode.getTextContent();

					//
					TypeC curC = new TypeC();
					curC.setD(textD);
					curC.setX(x);

					cs.add(curC);
				}

			}
		}

		return a;
	}

}
